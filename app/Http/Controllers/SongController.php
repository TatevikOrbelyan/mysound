<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Songs;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class SongController extends Controller
{
    public function index(){
        $results = Songs::all();
        return view('welcome', ['results' => $results ]);
    }

    public function create(Request $request){
        $songs_name = $request->input('songs_name');
        $songs_author = $request->input('songs_author');
        $file = $request->file('songs_image');
        $fileName = $file->getClientOriginalName();
        $result = New Songs;
        $result->songs_name = $songs_name;
        $result->songs_author = $songs_author;
        $result->songs_image = $fileName;
        $result->save();

        $destinationPath = base_path('storage\uploads');
        $file->move($destinationPath, $fileName);
        return redirect()->action('SongController@index');
    }
    public function update($id){
        $result=Songs::find($id);
        return view('edit', ['result' => $result]);
    }

    public function edit(Request $request, $id){
         $result=Songs::find($id);
         $result->songs_name = $request->input('songs_name');
         $result->songs_author = $request->input('songs_author');
          $result->save();
        return redirect()->action('SongController@index');
    }

public function delete($id){
       $results = Songs::find($id)->delete();
          return redirect('/index');
}
}


