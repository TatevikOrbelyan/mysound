<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
Route::get('edit', 'HomeController@edit');

Route::get('admin/scrap/index', 'ScrapController@index');

Route::get('admin/scrap', 'ScrapController@index');

Route::get('admin/scrap/pachd', 'ScrapController@pachd');

Route::get('/index', 'SongController@index');
Route::post('/create', 'SongController@create');

Route::get('/update/{id}', ['as'=>'update', 'uses' => 'SongController@update']);
Route::post('/edit/{id}', ['as' => 'edit', 'uses' =>'SongController@edit']);
Route::any('/index/{id}',['as'=>'delete', 'uses'=>'SongController@delete']);



//Auth::routes();
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
