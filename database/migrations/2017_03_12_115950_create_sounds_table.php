<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sounds', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('categoryId');
                        $table->string('filetypeId');
                        $table->string('name');
                        $table->string('duration');
                        $table->string('bitrate');
                        $table->string('samplerate');
                        $table->string('path');
			$table->timestamps();
                        
                        /*$table->foreign('categoryId')
                                ->references('id')->on('categories')
                                ->onDelete('cascade');
                        
                        $table->foreign('filetypeId')
                                ->references('id')->on('filetypes')
                                ->onDelete('cascade');*/
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sounds');
	}

}
