@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{route('edit', $result->id)}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="text">Songs ID:</label>
                                <input type="text" name="songs_name" class="form-control" value="{{$result->id}}" placeholder="Enter a songs_name">
                            </div>

                            <div class="form-group">
                                <label for="text">Songs Name:</label>
                                <input type="text" name="songs_name" class="form-control" value="{{$result->songs_name}}" placeholder="Enter a songs_name">
                            </div>
                            <div class="form-group">
                                <label for="text">Songs Author:</label>
                                <input type="text" name="songs_author"  class="form-control" value="{{$result->songs_author}}" placeholder="Enter a songs_author">
                            </div>

                            <div class="form-group">
                                <label for="text">Songs File:</label>
                                <input type="file" name="songs_image"  class="form-control" placeholder="Enter a songs_file" >
                            </div>
                            <button type="submit" class="btn btn-warning">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection;