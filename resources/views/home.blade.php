@extends('app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Dashboard</div>

					<div class="panel-body">
						<form action="create" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="text">Songs Name:</label>
								<input type="text" name="songs_name" class="form-control" placeholder="Enter a songs_name">
							</div>

							<div class="form-group">
								<label for="text">Songs Author:</label>
								<input type="text" name="songs_author"  class="form-control" placeholder="Enter a songs_author">
							</div>

							<div class="form-group">
								<label for="text">Songs File:</label>
								<input type="file" name="songs_image"  class="form-control" placeholder="Enter a songs_file" >
							</div>
							<button type="submit">Apply</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
