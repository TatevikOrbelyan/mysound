<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MyBlog</title>
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="../public/css/style.css">
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/js.js" type="text/javascript"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
	@if (Route::has('login'))
		<div class="top-right links">
			@if (Auth::check())
				<a href="{{ url('/home') }}">Home</a>
			@else
				<a href="{{ url('/login') }}">Login</a>
				<a href="{{ url('/register') }}">Register</a>
			@endif
		</div>
	@endif


	<div class="main_content">
		<a href="{{url('/home')}}">Home</a>
		<h2 style="text-align: center ; color: red">Songs List</h2>
		<div class="wrapper">
			<span class="count_span">{{$i=1}}</span>
			@foreach($results as $data)
				<span class="first-span">
                              {{$data->songs_name}}
								{{$data->songs_author}}
					<br>
								<audio controls>
                               <source src="../storage/uploads/{{$data->songs_image}}">
                           </audio>
                           <a href="../storage/uploads/{{$data->songs_image}}" download="{{$data->songs_image}}"></a>
                       </span>
							<div class="buttons">
								<div>
									<a href="{{route('update', $data->id)}}"  class="btn btn-success" >
										<i class="icon-pencil icon-white">Edit</i>
									</a>
								</div>

                                     <div id="delete">
										 <a href="{{route('delete', $data->id)}}" class="btn btn-success">
											 <i class="icon-pencil icon-white">Delete</i>
										 </a>
									 </div>

								<div class="clear"></div>
							</div>


				@if($i<count($results))
					<span class="count_span">{{++$i}}</span>
				@endif
			@endforeach
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>





